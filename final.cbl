       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL-EXAM.
       AUTHOR. SIRAPATSON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader4.dat" 
                    ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-FILE.
       01  TRADER-LIST.
           88 END-OF-TRADER-FILE VALUE HIGH-VALUES.
           05 PROVINCE       PIC 9(2).
           05 TRADER-ID      PIC 9(4).
           05 TRAD-INCOME    PIC 9(6).

       WORKING-STORAGE SECTION. 
       01  REPORT-HEADING    PIC X(47) 
           VALUE "PROVINCE     P INCOME     MEMBER  MEMBER INCOME".

       01  DETAIL-LINE.
           05 PRN-PROVINCE   PIC BZ9.
           05 PRN-P-INCOME   PIC B(6)$$,$$$,$$$,$$9.
           05 PRN-MEMBER-COUNT  PIC B(3)ZZ,ZZ9.
           05 PRN-MEMBER-INCOME PIC B(5)$$$,$$$,$$9.

       01  TOTALS-RESULT.
           05 PRN-MAX-PROVINCE  PIC 9(3).
           05 PRN-SUM-INCOME    PIC BBBB$$$,$$$,$$9.
           05 PRO-COUNT         PIC 9(3).
           05 MEM-COUNT         PIC 9(3).
           05 SUM-MEM-INCOME    PIC $$$,$$$,$$9.
           05 SUM-TRAD-INCOME   PIC $,$$$,$$$,$$9.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-FILE 
      *    DISPLAY REPORT-HEADING 

           READ TRADER-FILE 
              AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ
           
           PERFORM UNTIL END-OF-TRADER-FILE
              ADD TRAD-INCOME TO SUM-TRAD-INCOME(PROVINCE)
              READ TRADER-FILE 
                 AT END SET END-OF-TRADER-FILE TO TRUE
              END-READ
           END-PERFORM

           DISPLAY "**********************************"
           
           PERFORM VARYING PROVINCE FROM 1 BY 1 UNTIL END-OF-TRADER-FILE 
              MOVE SUM-MEM-INCOME(PROVINCE) TO PRN-SUM-INCOME
              DISPLAY "ID " PROVINCE " SUM INCOME " PRN-SUM-INCOME
           END-PERFORM

           CLOSE TRADER-FILE
           STOP RUN
           .

      *PROCESS-MAX-PROVINCE.
      *    MOVE ZEROS TO PRO-COUNT
      *    PERFORM PROVINCE UNTIL END-OF-TRADER-FILE
              
      *    .